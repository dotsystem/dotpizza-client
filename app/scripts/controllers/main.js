'use strict';

/**
 * @ngdoc function
 * @name dotpizzaClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dotpizzaClientApp
 */
angular.module('dotpizzaClientApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
