'use strict';

/**
 * @ngdoc function
 * @name dotpizzaClientApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the dotpizzaClientApp
 */
angular.module('dotpizzaClientApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
